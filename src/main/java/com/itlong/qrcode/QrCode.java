package com.itlong.qrcode;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.itlong.entity.QrCodeSDKDTO;
import com.itlong.entity.QrCodeSDKPermissionDTO;
import com.itlong.util.StringHandlerUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <desc>
 * 二维码
 * </desc>
 *
 * @createDate 2022/02/22
 */
public class QrCode {


    /**
     * <desc>
     * 生成二维码
     * </desc>
     *
     * @param qrCode
     * @return
     * @author Juguang.S
     * @createDate 2022/02/22
     */
    public Map getQrCode(String qrCode, String appid, String appsecret, String projectId, String projectKey) {
        QrCode1 qrCode1 = new QrCode1();
        return qrCode1.getQrCode(qrCode, appid, appsecret, projectId, projectKey);
    }


}
