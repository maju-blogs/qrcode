package com.itlong.qrcode;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.itlong.entity.QrCodeSDKDTO;
import com.itlong.entity.QrCodeSDKPermissionDTO;
import com.itlong.util.StringHandlerUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <desc>
 * 二维码
 * </desc>
 *
 * @createDate 2022/02/22
 */
public class QrCode1 {

    public static Logger log = Logger.getLogger(QrCode1.class);

    /**
     * <desc>
     * 生成二维码
     * </desc>
     *
     * @param qrCode
     * @return
     * @author Juguang.S
     * @createDate 2022/02/22
     */
    public Map<Object, Object> getQrCode(String qrCode, String appid, String appsecret, String projectId, String projectKey) {
        try {
            log.info("================================getQrCode==============================");
            Map<Object, Object> resultMap = new HashMap<Object, Object>();
            if (StringUtils.isEmpty(projectId) || StringUtils.isBlank(qrCode) || StringUtils.isBlank(appid) || StringUtils.isBlank(appsecret) || StringUtils.isBlank(projectKey)) {
                resultMap.put("code", "1001");
                return resultMap;
            }

            QrCodeSDKDTO qrCodeSDKDTO;
            try {
                qrCodeSDKDTO = new QrCodeSDKDTO(JSON.parseObject(qrCode));
            } catch (Exception e) {
                resultMap.put("code", "1002");
                e.printStackTrace();
                return resultMap;
            }
            if (!qrCodeSDKDTO.getUserId().matches("[0-9]{8}")) {
                resultMap.put("code", "1003");
                return resultMap;
            } else if (!qrCodeSDKDTO.getBeginDate().matches("[0-9]{10}")) {
                resultMap.put("code", "1004");
                return resultMap;
            } else if (!qrCodeSDKDTO.getValidityDuration().matches("[0-9]*") || !(Integer.valueOf(qrCodeSDKDTO.getValidityDuration()) <= 1440)) {
                resultMap.put("code", "1005");
                return resultMap;
            } else if (!qrCodeSDKDTO.getPermissCount().matches("[0-9]*") || !(Integer.valueOf(qrCodeSDKDTO.getPermissCount()) <= 255)) {
                resultMap.put("code", "1006");
                return resultMap;
            }

            Map<String, Object> result = secretNewQrCodeStr(qrCode, projectKey);
            if (result.get("secretQrcode").toString().equals("1")) {
                resultMap.put("code", "1007");
                return resultMap;
            } else if (result.get("secretQrcode").toString().equals("2")) {
                resultMap.put("code", "1008");
                return resultMap;
            }
            resultMap.put("code", "1000");
            resultMap.put("secretQrcode", result.get("secretQrcode"));
            return resultMap;
        } catch (Exception e) {

            log.error("生成二维码失败", e);
        }
        return null;
    }

    /**
     * <desc>
     * 生成新版sdk二维码
     * </desc>
     *
     * @param qrCode
     * @return
     * @author Juguang.S
     * @createDate 2022/02/22
     */
    public Map<String, Object> secretNewQrCodeStr(String qrCode, String projectKey) throws Exception {
        Map<String, Object> result = new HashMap<>();
        QrCodeSDKDTO qrCodeSDKDTO = new QrCodeSDKDTO(JSON.parseObject(qrCode));
        StringBuilder heard = new StringBuilder("SDK:{L:");
        StringBuilder content = new StringBuilder();
        //协议版本 1Byte, Hex码
        content.append("V:" + "10" + ",");
        //用户uid
        content.append("U:" + qrCodeSDKDTO.getUserId() + ",");
        //类型 访客为44 业主为43 物业为42
        content.append("M:");
        if (qrCodeSDKDTO.getType().equals("1")) {
            content.append("53,");
        } else if (qrCodeSDKDTO.getType().equals("2")) {
            content.append("54,");
        } else if (qrCodeSDKDTO.getType().equals("3")) {
            content.append("52,");
        } else {
            result.put("secretQrcode", 1);
            return result;  //二维码类型错误
        }
        //房间编号 固定00
        content.append("H:00,");
        //批次号
        content.append("I:" + StringHandlerUtil.autlSupply(4, Integer.toHexString(Integer.valueOf(qrCodeSDKDTO.getBatch())), "0") + ",");
        //启用时间
        content.append("T:" + qrCodeSDKDTO.getBeginDate() + ",");
        //有效时间段
        String startTime = qrCodeSDKDTO.getBeginDate();
        content.append("P:" + StringHandlerUtil.autlSupply(4, Integer.toHexString(Integer.valueOf(qrCodeSDKDTO.getValidityDuration())).toUpperCase(), "0") + ",");
        //有效次数
        String validCount = StringHandlerUtil.autlSupply(4, qrCodeSDKDTO.getValidityTime() != null ? Integer.toHexString(qrCodeSDKDTO.getValidityTime()) : "01", "0").toUpperCase();
        content.append("S:" + validCount + ",");
        if (validCount.length() > 2) {
            validCount = validCount.substring(2, 4);
        }
        String value = startTime + StringHandlerUtil.autlSupply(4, Integer.toHexString(Integer.valueOf(qrCodeSDKDTO.getValidityDuration())).toUpperCase(), "0") + validCount;

        if ("255".equals(qrCodeSDKDTO.getPermissCount())) {
            content.append("Q:{N3:0,X3:0},");
        } else {
            if (qrCodeSDKDTO.getPermission() != null && qrCodeSDKDTO.getPermission().size() < 255) {
                //权限组
                for (QrCodeSDKPermissionDTO permission : qrCodeSDKDTO.getPermission()) {
                    content.append("Q:{");
                    //权限标识
                    content.append("N1:" + StringHandlerUtil.autlSupply(2, Integer.toHexString(Integer.valueOf(permission.getPermissionMark())).toUpperCase(), "0") + ",");
                    //楼层权限标识 楼层权限
                    if (permission.getPermissionType().equals("2")) {
                        content.append("X3:00,");
                        if (StringUtils.isBlank(permission.getDirectFloor())) {
                            content.append("Y:" + "00");
                        } else {
                            content.append("Y:" + StringHandlerUtil.autlSupply(2, Integer.toHexString(Integer.valueOf(permission.getDirectFloor())).toUpperCase(), "0"));
                        }
                        content.append("},");
                        continue;
                    }
                    if (permission.getPermissionType().equals("0")) {
                        content.append("X2:00,Y:00");
                        content.append("},");
                        continue;
                    }
                    //正常楼层数量
                    String[] floor = StringHandlerUtil.splitString(permission.getPermissionFloor());
                    content.append("X2:" + StringHandlerUtil.assembleNewDeviceNum(floor) + ",");
                    //自动登记楼层
                    if (StringUtils.isBlank(permission.getDirectFloor())) {
                        content.append("Y:" + "00");
                    } else {
                        content.append("Y:" + StringHandlerUtil.autlSupply(2, Integer.toHexString(Integer.valueOf(permission.getDirectFloor())).toUpperCase(), "0"));
                    }
                    content.append("},");
                }
            }
        }

        String key = getUserKey(qrCodeSDKDTO.getUserId() + "00000000", projectKey);
        //计算mac
        String mac = "K:" + newOwnerAndVisitorMacEncrypt(value, key, content.toString());
        String qrcode = heard.toString() + StringHandlerUtil.autlSupply(4, Integer.toHexString((content + mac).length()).toUpperCase(), "0") + "," + content + mac + "}";
        result.put("secretQrcode", qrcode);
        return result;
    }


    /**
     * <desc>
     * userKey
     * </desc>
     *
     * @param datasource String userId
     * @param password   String projectKey
     * @return
     * @author Jiaqi.X
     * @createDate 2018/05/25
     */
    public static String getUserKey(String datasource, String password) {
        try {
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(hexStrToBytes(password));
            //创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            //Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            //用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            //现在，获取数据并加密
            //正式执行加密操作
            return bytesToHexStr(cipher.doFinal(hexStrToBytes(datasource))).substring(0, 16);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * <desc>
     * 把16进制字符串转换成字节数组.
     * </desc>
     *
     * @param hex 待转换的Hex字符串
     * @return 转换得到的byte数组
     * @author LewisLouis
     * @createDate 2017-11-10
     */
    public static byte[] hexStrToBytes(String hex) {
        int len = (hex.length() / 2);
        byte[] result = new byte[len];
        char[] achar = hex.toCharArray();
        for (int i = 0; i < len; i++) {
            int pos = i * 2;
            result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
        }
        return result;
    }

    /**
     * <desc>
     * 将单个十六进制字符转换为字节
     * </desc>
     *
     * @param c 单个字符
     * @return 转换得到的byte
     * @author LewisLouis
     * @createDate 2017-11-10
     */
    private static byte toByte(char c) {
        byte b = (byte) "0123456789ABCDEF".indexOf(c);
        return b;
    }

    /**
     * <desc>
     * 把字节数组转换成16进制字符串.
     * </desc>
     *
     * @param bArray 待转换的byte数组
     * @return 转换得到的Hex字符串
     * @author LewisLouis
     * @createDate 2017-11-10
     */
    public static String bytesToHexStr(byte[] bArray) {
        if (bArray == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer(bArray.length);
        String sTemp;
        for (int i = 0; i < bArray.length; i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * <desc>
     * 获取授权二维码Mac值
     * MAC算法:
     * 将字符串qrcode分为8字节为单位的数据块，不足补\x00,分别标号为D1,D2,D3,...,Dn
     * 设置初始向量E0="\x00\x00\x00\x00\x00\x00\x00\x00"
     * 将E0^D1 —---->E1(E0,D1异或的后结果经des加密得到E1)
     * 将E1^D2 ----->E2
     * 如此类推，知道得出En结束，En即是计算出来的MAC
     * </desc>
     *
     * @param value  采用二维码中 “生成日期（5Byte：年/月/日/时/分）+流水号（1Byte） +有效时间（1Byte：*10分钟）+有效次数（1Byte）”8Byte数据作为初始向量
     * @param key    项目秘钥
     * @param qrcode 二维码数据
     * @return
     * @author Jiaqi.X
     * @createDate 2017/12/20
     */
    public static String newOwnerAndVisitorMacEncrypt(String value, String key, String qrcode) throws NoSuchProviderException {
        try {
            qrcode = stringToAscii(qrcode);
            System.out.println(qrcode);
            while (qrcode.length() % 16 != 0) {
                qrcode += "0";
            }
            List<byte[]> bytes = new ArrayList<byte[]>();
            for (int i = 0; i < qrcode.length() / 16; i++) {
                bytes.add(hexStrToBytes(qrcode.substring(i * 16, i * 16 + 16)));
            }
            byte[] vi = hexStrToBytes(value);
            byte[] bKey = hexStrToBytes(key);
            String ds = "";
            for (byte[] bt : bytes) {
                for (int i = 0; i < bt.length; i++) {
                    String ik = hexString2binaryString(bytesToHexStr(new byte[]{bt[i]}));
                    String pro = hexString2binaryString(bytesToHexStr(new byte[]{vi[i]}));
                    vi[i] = hexStrToBytes(binaryString2hexString(xor(ik, pro)).toUpperCase())[0];
                }
                // 创建一个DESKeySpec对象
                SecretKeySpec spec = new SecretKeySpec(bKey, "DES");
                // Cipher对象实际完成加密操作
                try {
                    Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
                    // 实例化cipher
                    cipher.init(Cipher.ENCRYPT_MODE, spec);
                    // 开始加密
                    vi = cipher.doFinal(vi);
                    ds = bytesToHexStr(vi);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                }
            }
            return ds.toUpperCase().substring(0, 8);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * <desc>
     * 字符串转ascii码
     * </desc>
     *
     * @param value
     * @return
     * @author Jiaqi.X
     * @createDate 2019/10/16
     */
    public static String stringToAscii(String value) {
        StringBuilder sbu = new StringBuilder();
        char[] chars = value.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i != chars.length - 1) {
                sbu.append((int) chars[i]).append(",");
            } else {
                sbu.append((int) chars[i]);
            }
        }
        String[] str = StringHandlerUtil.splitString(sbu.toString());
        StringBuilder result = new StringBuilder();
        for (String s : str) {
            result.append(StringHandlerUtil.autlSupply(2, Integer.toHexString(Integer.valueOf(s)), "0").toUpperCase());
        }
        return result.toString();
    }

    /**
     * <desc>
     * 十六进制转二进制
     * </desc>
     *
     * @param hexString
     * @return
     * @author Jiaqi.X
     * @createDate 2017/12/20
     */
    public static String hexString2binaryString(String hexString) {
        try {
            if (hexString == null || hexString.length() % 2 != 0)
                return null;
            String bString = "", tmp;
            for (int i = 0; i < hexString.length(); i++) {
                tmp = "0000"
                        + Integer.toBinaryString(Integer.parseInt(hexString
                        .substring(i, i + 1), 16));
                bString += tmp.substring(tmp.length() - 4);
            }
            return bString;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * <desc>
     * 二进制转十六进制
     * </desc>
     *
     * @param bString
     * @return
     * @author Jiaqi.X
     * @createDate 2017/12/20
     */
    public static String binaryString2hexString(String bString) {
        try {
            if (bString == null || bString.equals("") || bString.length() % 8 != 0)
                return null;
            StringBuffer tmp = new StringBuffer();
            int iTmp = 0;
            for (int i = 0; i < bString.length(); i += 4) {
                iTmp = 0;
                for (int j = 0; j < 4; j++) {
                    iTmp += Integer.parseInt(bString.substring(i + j, i + j + 1)) << (4 - j - 1);
                }
                tmp.append(Integer.toHexString(iTmp));
            }
            return tmp.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * <desc>
     * 将两个二进制数做异或运算
     * </desc>
     *
     * @param anotherBinary
     * @param thisBinary
     * @return
     * @author Jiaqi.X
     * @createDate 2017/12/20
     */
    private static String xor(String anotherBinary, String thisBinary) {
        try {
            String result = "";
            //判断是否为8位二进制，否则左补零
            if (anotherBinary.length() != 8) {
                for (int i = anotherBinary.length(); i < 8; i++) {
                    anotherBinary = "0" + anotherBinary;
                }
            }
            if (thisBinary.length() != 8) {
                for (int i = thisBinary.length(); i < 8; i++) {
                    thisBinary = "0" + thisBinary;
                }
            }
            //异或运算
            for (int i = 0; i < anotherBinary.length(); i++) {
                //如果相同位置数相同，则补0，否则补1
                if (thisBinary.charAt(i) == anotherBinary.charAt(i))
                    result += "0";
                else {
                    result += "1";
                }
            }
            return result;
        } catch (Exception e) {
            throw e;
        }
    }


}
