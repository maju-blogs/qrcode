package com.itlong.entity;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

/**
 * <desc>
 * 二维码权限组DTO类
 * </desc>
 *
 * @createDate 2018/05/24
 */
public class QrCodeSDKPermissionDTO implements Serializable {
    private String permissionMark;  //权限组标识 客户自定义 0-255之间
    private String permissionType;  //楼层权限标识 1有权限 2表示有所有楼层权限
    private String permissionFloor; //楼层权限
    private String directFloor;     //直达楼层

    public QrCodeSDKPermissionDTO(JSONObject jsonObject) {
        this.permissionMark = jsonObject.getString("permissionMark");
        this.permissionType = jsonObject.getString("permissionType");
        this.permissionFloor = jsonObject.getString("permissionFloor");
        this.directFloor = jsonObject.getString("directFloor");
    }

    public String getPermissionMark() {
        return permissionMark;
    }

    public void setPermissionMark(String permissionMark) {
        this.permissionMark = permissionMark;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    public String getPermissionFloor() {
        return permissionFloor;
    }

    public void setPermissionFloor(String permissionFloor) {
        this.permissionFloor = permissionFloor;
    }

    public String getDirectFloor() {
        return directFloor;
    }

    public void setDirectFloor(String directFloor) {
        this.directFloor = directFloor;
    }
}
