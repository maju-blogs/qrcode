package com.itlong.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <desc>
 * 二维码SDK DTO类
 * </desc>
 *
 * @createDate 2018/05/24
 */
public class QrCodeSDKDTO implements Serializable {
    private String version;     //版本
    private String type;        //类型 1，业主 2，访客
    private String appid;       //appid
    private String appsecret;   //appsecret
    private String batch;         //批次号
    private String userId;      //用户id
    private String beginDate;  //启用日期 年月日时分 2018-05-·08 08:16 1805081600
    private String validityDuration;    //有效期 单位分钟
    private Integer validityTime;        //有效次数 预留 默认为0
    private String permissCount;        //权限组数量 255表示有所有权限
    private List<QrCodeSDKPermissionDTO> permission;    //权限组
    private String projectId;
    //项目id

    public QrCodeSDKDTO(JSONObject json) {
        this.version = json.getString("version");
        this.type = json.getString("type");
        this.appid = json.getString("appid");
        this.appsecret = json.getString("appsecret");
        this.batch = json.getString("batch");
        this.userId = json.getString("userId");
        this.beginDate = json.getString("beginDate");
        this.validityDuration = json.getString("validityDuration");
        this.validityTime = json.getInteger("validityTime");
        this.permissCount = json.getString("permissCount");
        this.permission = getPermission(json.getJSONArray("permission"));
        this.projectId = json.getString("projectId");
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppsecret() {
        return appsecret;
    }

    public void setAppsecret(String appsecret) {
        this.appsecret = appsecret;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getValidityDuration() {
        return validityDuration;
    }

    public void setValidityDuration(String validityDuration) {
        this.validityDuration = validityDuration;
    }

    public Integer getValidityTime() {
        return validityTime;
    }

    public void setValidityTime(Integer validityTime) {
        this.validityTime = validityTime;
    }

    public String getPermissCount() {
        return permissCount;
    }

    public void setPermissCount(String permissCount) {
        this.permissCount = permissCount;
    }

    public List<QrCodeSDKPermissionDTO> getPermission() {
        return permission;
    }

    public void setPermission(List<QrCodeSDKPermissionDTO> permission) {
        this.permission = permission;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    private List<QrCodeSDKPermissionDTO> getPermission(JSONArray permission) {
        if (null == permission) {
            return null;
        }
        List<QrCodeSDKPermissionDTO> list = new ArrayList<>();
        for (int i = 0; i < permission.size(); i++) {
            QrCodeSDKPermissionDTO dto = new QrCodeSDKPermissionDTO(permission.getJSONObject(i));
            list.add(dto);
        }
        return list;
    }

}
