package com.itlong.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class StringHandlerUtil {


    /**
     * <desc>
     * 自动填充字符串
     * </desc>
     *
     * @param length 长度
     * @param str    字符串
     * @param supply 待填充的字符
     * @return
     * @author Jiaqi.X
     * @createDate 2017/09/27
     */
    public static String autlSupply(int length, String str, String supply) {
        StringBuffer sb = new StringBuffer(str);
        while (sb.length() < length) {
            sb.insert(0, supply);
        }
        return sb.toString();
    }

    /**
     * <desc>
     *      获取组装后的机号或楼层数据
     * </desc>
     *
     * @param content
     * @return
     * @author Jiaqi.X
     * @createDate 2019/10/16
     */
    public static String assembleNewDeviceNum(String[] content){
        try {
            List<Integer> list = new ArrayList<Integer>();
            Integer max = null;
            for (String con : content) {
                Integer c = Integer.valueOf(con);
                list.add(c);
                if (max == null) {
                    max = c;
                } else if (c > max) {
                    max = c;
                }
            }
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= max; i++) {
                Integer n = i;
                if (list.stream().filter(num -> num.equals(n)).findFirst().isPresent()) {
                    result.append("1");
                } else {
                    result.append("0");
                }
            }
            String res = result.toString();
            while (StringUtils.isBlank(res) || (res.length() % 8 != 0)) {
                res += "0";
            }
            return binaryToHex(res).toUpperCase();
        }catch (Exception e){
            System.out.println(content.toString());
            throw e;
        }
    }


    /**
     * <desc>
     *      进制转换
     * <desc/>
     *
     * @param str 需转换的字符串
     * @return
     * @author zhangs
     * @createDate 2017/10/10
     */
    public static String binaryToHex(String str)
    {
        if ((StringUtils.isBlank(str)) || (str.length() % 8 != 0)) {
            return null;
        }
        StringBuffer tmp = new StringBuffer();
        int iTmp = 0;
        for (int i = 0; i < str.length(); i += 4) {
            iTmp = 0;
            for (int j = 0; j < 4; j++) {
                iTmp += Integer.parseInt(str.substring(i + j, i + j + 1)) << (4 - j - 1);
            }
            tmp.append(Integer.toHexString(iTmp));
        }
        return tmp.toString();
    }


    /**
     * <desc>
     * 将,号分隔的字符串转换为数组
     * </desc>
     *
     * @param str
     * @return
     * @author Jiaqi.X
     * @createDate 2017/09/21
     */
    public static String[] splitString(String str) {
        int strLen;
        if (str != null && (strLen = str.length()) != 0) {
            if (!str.substring(0, strLen - 1).equals(",")) {
                return str.split(",");
            } else {
                return str.substring(0, strLen - 2).split(",");
            }
        } else {
            return new String[]{};
        }
    }
}